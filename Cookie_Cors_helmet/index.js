const express = require('express')
const app = express()
const cookieParser = require('cookie-parser')
const helmet = require("helmet");

const cors = require('cors')

//using Helmet for hinding Important Headers.
app.use(helmet());

app.use(cookieParser()) // using Cookie

//Cors Setting for API
app.use(cors({
    origin: '*'
}));

//default route
app.get('/',(req,res)=>{

    res.send('Server Get Requested')
})

//basic data for user
let user ={
    name:'vivek',
    age:'18'
}

//route for setting user
app.get('/setuser',(req,res)=>{
    res.cookie("userData",user)
    res.send('Data Added')
})

//route for clearing cookie
app.get('/clearcookie',(req,res)=>{
    res.clearCookie('userData');
    res.send('Cookies Reset')
})

//getting users
app.get('/getUser',(req,res)=>{
        res.send(req.cookies)
})


//listening server
app.listen(3000,()=>{
    console.log('Server Initiated');
})