require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser');
const app  =  express();
const jwt = require("jsonwebtoken")

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json())

//sample data for user with post
const post = [
    {
        name:'kyle',
        title:'post 1'
    },
    {
        name:'james',
        title:'post 2'
    }
]
//middle ware for credential validation.
const auth = (req,res,next)=>{
    const {username,password} = req.body;
    if(username == "vivek" && password == "Apstndp"){
        next();
    }else{
        res.send("UnAuthorised User")
    }
}

//validating jwt token
const authentication = (req,res,next)=>{
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    jwt.verify(token,process.env.ACCES_TOKEN_SECRET,(err,user)=>{
        if(err) return res.sendStatus(403)
        req.user = user
        next() 
    })
}
//requesting post saprated by user name
app.get("/post",authentication,(req,res)=>{
    res.json(post.filter(post=>post.name === req.body.username))
})

//route for loging
app.post("/login",auth,(req,res)=>{
    const username  =  req.body.username;
    const user = { name : username}
    const accesToken = jwt.sign(user,process.env.ACCES_TOKEN_SECRET)
    res.json({accesToken:accesToken})    
})

//listening server.
app.listen(3000,()=>{
    console.log("Intiated");
})