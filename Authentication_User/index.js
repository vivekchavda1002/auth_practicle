const express = require('express')
const bodyParser = require('body-parser');
const app  =  express();

//middle ware for Credential checking
const auth = (req,res,next)=>{    
    const {username,password} = req.body;
    if(username == "vivek" && password == "Apstndp"){
        next();
    }else{
        res.send("UnAuthorised User")
    }
}

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json())

//route for validating user.
app.get("/auth",auth,(req,res)=>{
    res.send("Valid user.")    
})

//server listening.
app.listen(3000,()=>{
    console.log("Intiated");
})